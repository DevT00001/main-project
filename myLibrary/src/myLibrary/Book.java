package myLibrary;

public class Book {
	private String title;
	private String auther;
	private String rating;
	
	//CLASS CONSTRUCTOR
	public Book(){
		
	}
	
	//METHODS TO GET AND SET TITLE, AUTHER, RATING
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String setTitle) {
		this.title = setTitle;
	}
	
	public String getAuther() {
		return auther;
	}
	
	public void setAuther(String setAuther) {
		this.auther = setAuther;
	}
	
	public String getRating() {
		return rating;
	}
	
	public void setRating(String setRating) {
		this.rating = setRating;
	}
	
	@Override
	public String toString() {
		return "Book [title: " + title + ", auther: " + auther + ", rating: " + rating + "]";
	}
}
