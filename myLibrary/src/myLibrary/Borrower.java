package myLibrary;

import java.util.List;

public class Borrower {
	private String name;
	private int ID;
	private int age;
	private List<Book> booklist;
	
	//CLASS CONSTRUCTOR
	public Borrower(){
		
	}
	
	//METHODS TO GET AND SET NAME, ID, AGE, BOOKLIST
	public String getName(){
		return name;
	}
	
	public void setName(String setName){
		this.name = setName;
	}
	
	public int getID(){
		return ID;
	}
	
	public void setID(int setID){
		this.ID = setID;
	}
	
	public int getAge(){
		return age;
	}
	
	public void setAge(int setAge){
		this.age = setAge;
	}
	
	public List<Book> getBookList() {
		return booklist;
	}
	
	public void setBookList(List<Book> setBooklist) {
		this.booklist = setBooklist;
	}
	
	@Override
    public String toString() {
		return "Borrower [name: " + name + ", ID: " + ID + ", age: " + age + ", " + booklist + "]";
	}

	public String toFile() {
		String booklistStr = "";
		if (booklist != null) {
			for (int i = 0; i < booklist.size(); i++) {
				booklistStr = booklistStr + booklist.get(i).getTitle() + ", " + booklist.get(i).getAuther() + ", " + booklist.get(i).getRating();
				if (i != booklist.size() - 1 ) {
					booklistStr = booklistStr + ",";
				}
			}
		}
		return name + ", " + ID + ", " + age + ", " + booklistStr;
	}
}
