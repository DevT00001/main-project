package myLibrary;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	//BORROWER LIST FORMAT: BORROWER NAME, BORROWER ID, BORROWER AGE, BOOK TITLE N, BOOK AUTHER N, BOOK RATING N
	public static List<Borrower> borrowerList = new ArrayList<Borrower>(); 
	//LIBRARY INVERNTORY LIST FORMAT: BOOK TITLE N, BOOK AUTHER N, BOOK RATING N
	public static List<Book> bookInventoryList = new ArrayList<Book>();
	
	public static void main(String[] args) {
		
		readDataFile();
		while(true) {
			String getInput = showMenu();
			try {
				switch (Integer.parseInt(getInput)) {
				case 1:
					registerBorrower();
					break;
				case 2:
					manageBorrower();
					break;
				case 3:
					displayBorrowers();
					break;
				case 4:
					displayHelp();
					break;
				case 5:
					writeDataFile();
					exitLibrary();
					return;
				default:
					System.out.println("*ERROR: PLEASE CHOOSE A VALID OPTION (1-5)!");
					break;
				}
				
			} catch (Exception e) {
				System.out.println("*ERROR: PLEASE ENTER A VAILD OPTION (1-5)!");
			}
		}
		
	}
	
	//READ DATA FILE 'BORROWERS.TXT'
	public static void readDataFile(){
		try {
			//LOAD DATA FILE 'BORROWERS.TXT'
			Scanner in = new Scanner(new File("src/myLibrary/borrowers.txt"));
			while (in.hasNextLine()) {
				Borrower borrower = new Borrower();
				Book book = new Book(); 
				List<Book> bookList = new ArrayList<Book>();
				String[] str = in.nextLine().split(",");
				//GET BORROWERS' INFO FROM THE DATA FILE
				borrower.setName(str[0]);
				borrower.setID(Integer.parseInt(str[1]));
				borrower.setAge(Integer.parseInt(str[2]));
				//GET BOOKS' INFO FROM THE DATA FILE
				for (int i = 3; i < str.length; i++) {
					switch (i%3) {
					case 0:
						book.setTitle(str[i]);
						break;
					case 1:
						book.setAuther(str[i]);
						break;
					case 2:
						book.setRating(str[i]);
						bookList.add(book); //ADD TO BOOKLIST
						bookInventoryList.add(book); //ADD TO LIBRARY INVENTORY
						book = new Book();
						break;
					default:
						break;
					}
				}
				borrower.setBookList(bookList);
				borrowerList.add(borrower); 
				System.out.println(str);
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	 //WRITE BORROWERLIST INTO FILE 'BORROWER.TXT'
	public static void writeDataFile(){
		try {
			File file = new File("src/myLibrary/borrowers.txt");
			FileOutputStream fos = new FileOutputStream(file);
			OutputStreamWriter osw = new OutputStreamWriter(fos);
			BufferedWriter bw = new BufferedWriter(osw);
			for (int i = 0; i < borrowerList.size(); i++) {
				bw.write(borrowerList.get(i).toFile());
				bw.newLine();
			}
			bw.flush();
			bw.close();
			osw.close();
			fos.close();
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}
	
	//SHOW MAIN MENU
	public static String showMenu(){		
		System.out.println(" ");
		System.out.println("Welcome to the My Library");
		System.out.println("=========================");
		System.out.println("(1) Register New Borrower");
		System.out.println("(2) Manage Borrower");
		System.out.println("(3) List All Borrowers");
		System.out.println("(4) Display Help");
		System.out.println("(5) Exit Library");
		System.out.print("Choose an option: ");	
		Scanner sc0 = new Scanner(System.in);
		try {
			String option = sc0.next();
			return option;
		} catch (Exception e) {
			return null;
		}
	}
	
	//(1) REGISTER NEW BORROWER 
	public static void registerBorrower(){
		Scanner sc1 = new Scanner(System.in);
		Borrower borrower = new Borrower();
		boolean flag = false;
		boolean idFlag = true;
		//NAME INPUT
		while (!flag) {
			System.out.print("PLEASE ENTER YOUR NAME:");
			String tmpName = sc1.nextLine();
			if (!" ".equals(tmpName)) {
				borrower.setName(tmpName);
				flag = true;
			} else {
				System.out.println("*ERROR: YOUR NAME MUST NOT BE BLANK");
			}
		}
		flag = false;
		//ID INPUT
		while(!flag){
			System.out.print("PLEASE ENTER YOUR ID: ");
			try {
				int tmpId = Integer.parseInt(sc1.next());
				idFlag = true;
				for (int i = 0; i < borrowerList.size(); i++) {
					if (tmpId == borrowerList.get(i).getID()) {
						System.out.println("*ERROR: THIS ID HAS BEEN USED");
						idFlag = false;
						break;
					}
				}
				if (idFlag) {
					borrower.setID(tmpId);
					flag = true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		flag = false;
		//录入年龄
		while(!flag){
			System.out.print("Please Input Age :");
			try {
				int tmpAge = Integer.parseInt(sc1.next()); 
				if (tmpAge <= 5 || tmpAge >= 110) {
					System.out.println("*ERROR: YOUR AGE MUST BE BETWEEN 6 AND 109!");
				} else {
					borrower.setAge(tmpAge);
					flag = true;
					System.out.println("Success!");
					borrowerList.add(borrower);
				}
			} catch (Exception e) {
				System.out.println("*ERROR: YOUR AGE MUST BE A NUMBER!");
			}
		}
		return;
	}
	
	//(2) MANAGE BORROWER
	public static void manageBorrower() {
		Scanner sc2 = new Scanner(System.in);
		boolean flag = false;
		int tmpId = 0;
		Borrower borrower = new Borrower();
		//输入管理目标的ID
		while(!flag){
			System.out.print("PLEASE ENTER THE ID OF THE BORROWER:");
			tmpId = Integer.parseInt(sc2.next());
			try {
				for (int i = 0; i < borrowerList.size(); i++) {
					if (tmpId == borrowerList.get(i).getID()) {
						borrower = borrowerList.get(i);
						flag = true;
						break;
					}
				}
				if (!flag) {
					System.out.println("*ERROR: RESTULT NOT FOUND!");
				}
			} catch (Exception e) {
				System.out.print("ERROR: YOUR ID MUST BE A NUMBER");
			}
		}
		System.out.println("Hello, " + borrower.getName() + ", here you can manage your account by");
		while (true) {
			String getInput2 = showSecondMenu();
			try {
				switch (Integer.parseInt(getInput2)) {
				case 1:
					borrowBook(tmpId);
					break;
				case 2:
					returnBook(tmpId);
					break;
				case 3:
					listBooks(tmpId);
					break;
				case 4:
					return;
				default:
					System.out.println("*ERROR: PLEASE CHOOSE A VALID OPTION (1-4)!");
					break;
				}
				
			} catch (Exception e) {
				System.out.println("*ERROR: PLEASE ENTER A VAILD OPTION (1-4)!");
			}
		}
	}
	
	//(2.1) SHOW SECOND MENU
	public static String showSecondMenu() {
		System.out.println("(1) Borrow a book");
		System.out.println("(2) Return a book");
		System.out.println("(3) List borrowed books");
		System.out.println("(4) Back");
		System.out.print("Choose an option: ");
		Scanner sc3 = new Scanner(System.in);
		try {
			String option2 = sc3.next();
			return option2;
		} catch (Exception e) {
			return null;
		}
	}
	
	//(2.2) BORROW A BOOK
	public static void borrowBook(int borrowerId) {
		System.out.print("Please Enter the Titile of the Book You Want to Borrow: ");
		Scanner sc4 = new Scanner(System.in);
		Book newBorrowedBook = new Book();
		try {
			//记录借书信息
			newBorrowedBook.setTitle(sc4.nextLine());
			System.out.print("Please Enter the Author of this Book: ");
			newBorrowedBook.setAuther(sc4.nextLine());
			System.out.print("Please Enter the Rate of this Book: ");
			newBorrowedBook.setRating(sc4.next());
			
			for (int i = 0; i < borrowerList.size(); i++) {
				if (borrowerId == borrowerList.get(i).getID()) {
					//判断借书人的年龄是否可以有权限借阅该书
					if (borrowerList.get(i).getAge() < 18 && "Adult".equals(newBorrowedBook.getRating())) {
						System.out.print("*ERROR: YOU CANNOT BORROW THIS BOOK BEFORE YOU'RE 18 YEARS OLD !");
						return;
					}
					//把新书加入到对应的借书人的借书列表
					List<Book> temBookList = borrowerList.get(i).getBookList();
					if (temBookList == null) {
						temBookList = new ArrayList<Book>();
					}
					temBookList.add(newBorrowedBook);
					borrowerList.get(i).setBookList(temBookList);
					System.out.print("*INFO: THE BOOK IS SUCCESSFULLY BORROWED !");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//(2.3) RETURN A BOOK
	public static void returnBook(int borrowerId) {
		System.out.print("Please Enter the Title of the Book You Want to Return: ");
		Scanner sc5 = new Scanner(System.in);
		try {
			String returnBookTitle = sc5.nextLine();
			for (int i = 0; i < borrowerList.size(); i++) {
				if (borrowerId == borrowerList.get(i).getID()) {
					List<Book> temBookList = borrowerList.get(i).getBookList();
					boolean hitFlag = false;
					for (int j = 0; j < temBookList.size(); j++) {
						while(returnBookTitle.equals(temBookList.get(j).getTitle())) {
							hitFlag = true;
							temBookList.remove(j);
						}
					}
					if (hitFlag) {
						System.out.println("*INFO: THE BOOK IS SUCCESSFULLY RETURNED!");
					} else {
						System.out.println("*ERROR: THE BOOK IS NOT FOUND IN YOUR RECORD!");
					}
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//(2.4) LIST BORROWERD BOOKS
	public static void listBooks(int borrowerId) {
		try {
			for (int i = 0; i < borrowerList.size(); i++) {
				if (borrowerId == borrowerList.get(i).getID()) {
					System.out.println("*INFO: LOADING BOOKS YOU BORROWED...");
					for(int j = 0; j < borrowerList.get(i).getBookList().size(); j++) {
						System.out.println("Title: " + borrowerList.get(i).getBookList().get(j).getTitle() + ", Author: " + borrowerList.get(i).getBookList().get(j).getAuther() + ", Rating: " + borrowerList.get(i).getBookList().get(j).getRating());
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	//(3) DISPLAY ALL BORROWERS
	public static void displayBorrowers() {
		System.out.println("*INFO: DISPLAYING ALL BORROWERS AND BORROWED BOOKS...");
		for (int i = 0; i < borrowerList.size(); i++) {
			System.out.println(borrowerList.get(i));
		}
	}
	
	//(4) DISPLAY HELP
	public static void displayHelp() {
		System.out.println(" ");
		System.out.println("*INFO: DISPLAYING HELPDESK...");
		System.out.println("- IF YOU ARE NEW TO THE LIBRARY, PLEASE ENTER 1 TO REGISTER");
		System.out.println("- IF YOU HAS REGISTERED AND WANT TO BORROW, RETURN OR PRINT THE BOOKLIST OUT, PLEASE ENTER 2");
		System.out.println("- IF YOU WANT TO LIST ALL THE BORROWERS, PLEASE ENTER 3");
		System.out.println("- TO EXIT, PLEASE ENTER 5");
	}
	
	//(5) EXIT LIBRARY
	public static void exitLibrary() {
		System.out.println("*INFO: YOU ARE SUCCESSFULLY LOGGED OFF！");
	}
}
